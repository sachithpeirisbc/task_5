package Football;

public class FootballClub implements Comparable<FootballClub> {
  private int position;
  private String club;
  private int played;
  private int won;
  private int drawn;
  private int lost;
  private int goalScored;
  private int goalsAgainst;
  private int goaldifference;
  private int redCards;
  private int yellowCards;
  private int points;

  public FootballClub(int position, String club, int played, int won, int drawn,
      int lost, int goalScored, int goalsAgainst, int tablePosition,
      int redCards, int yellowCards, int points) {
    this.position = position;
    this.club = club;
    this.played = played;
    this.won = won;
    this.drawn = drawn;
    this.lost = lost;
    this.goalScored = goalScored;
    this.goalsAgainst = goalsAgainst;
    this.goaldifference = tablePosition;
    this.redCards = redCards;
    this.yellowCards = yellowCards;
    this.points = points;
  }

  public String toString() {
    return String.format("%-3d%-20s%10d%10d%10d", position, club, goalScored,
    		goalsAgainst, points);
  }

  public int getPosition() {
    return position;
  }

  public void setPosition(int position) {
    this.position = position;
  }

  public String getClub() {
    return club;
  }

  public void setClub(String club) {
    this.club = club;
  }

  public int getPlayed() {
    return played;
  }

  public void setPlayed(int played) {
    this.played = played;
  }

  public int getWon() {
    return won;
  }

  public void setWon(int won) {
    this.won = won;
  }

  public int getDrawn() {
    return drawn;
  }

  public void setDrawn(int drawn) {
    this.drawn = drawn;
  }

  public int getLost() {
    return lost;
  }

  public void setLost(int lost) {
    this.lost = lost;
  }

  public int getGoalScored() {
    return goalScored;
  }

  public void setGoalScored(int goalScored) {
    this.goalScored = goalScored;
  }

  public int getGoalsAgainst() {
    return goalsAgainst;
  }

  public void setGoalsAgainstt(int goalsAgainst) {
    this.goalsAgainst = goalsAgainst;
  }

  public int getPointsDifference() {
    return goaldifference;
  }

  public void setTablePosition(int tablePosition) {
    this.goaldifference = tablePosition;
  }

  public int getRedCards() {
    return redCards;
  }

  public void setRedCards(int redCards) {
    this.redCards = redCards;
  }

  public int getYellowCards() {
    return yellowCards;
  }

  public void setYellowCards(int yellowCards) {
    this.yellowCards = yellowCards;
  }


  public int getPoints() {
    return points;
  }

  public void setPoints(int points) {
    this.points = points;
  }
  
  public int compareTo(FootballClub c) {
    return ((Integer) points).compareTo(c.points);
  }
}
