package Football;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.OptionalInt;

public class Football05 {
  public static void main(String[] args) {
    List<FootballClub> table = Arrays.asList(
    		new FootballClub(1, "Real Madrid", 22, 20, 1, 1, 28, 9, 19,
    	            1, 15, 66),
    	        new FootballClub(2, "Wolverhampton Wanders", 22, 18, 0, 4, 20, 16, 4, 0, 8, 60),
    	        new FootballClub(3, "Leicester City", 22, 15, 6, 1, 18, 16, 2, 7,
    	            10, 58),
    	        new FootballClub(4, "Shefield United", 22, 14, 1, 7, 20, 20, 0, 1, 9, 54),
    	        new FootballClub(5, "Chelsea", 22, 14, 5, 3, 32, 20, 12, 3, 7,
    	            56),
    	        new FootballClub(6, "Liverpool", 22, 14, 1, 7, 13, 6, 7, 1, 12, 54),
    	        new FootballClub(7, "Sevilla", 22, 11, 0, 11, 39, 38, 1, 8, 16,
    	            54),
    	        new FootballClub(8, "AC Milan", 22, 10, 3, 9, 19, 14, 5, 0, 8,
    	            54),
    	        new FootballClub(9, "Nottingham Forest", 22, 9, 1, 12, 28, 20, 8, 3, 9,
    	            50),
    	        new FootballClub(10, "Tottenham", 22, 7, 1, 14, 22, 22, 0, 4, 7,
    	            49),
    	        new FootballClub(11, "Newcastle United", 22, 5, 1, 16, 9, 15, -6,
    	            4, 8, 42),
    	        new FootballClub(12, "Manchester United", 22, 0, 0, 22, 10, 21, -11, 2,
    	            9, 40));

    System.out.println("Sorted by Comparator in FootballClub class");
    table.stream().sorted().forEach(System.out::println);

    System.out.println();
    System.out.println("Sorted by lambda");
    table.stream()
         .sorted((c1, c2) -> 
            ((Integer) c1.getPointsDifference()).compareTo(c2.getPointsDifference()))
         .forEach(System.out::println);
    
    try {
	      FileWriter writer = new FileWriter("Output5.txt");
	      writer.write("Sorted by compare wining in FootballClub class\n\n");
	      writer.write("   Team Name                   won      drawn     lost     points\n");
	      writer.write("   ---------                   ---      ------    ----     ------\n");
	      table.stream().sorted()
	        .forEach(str -> {
	        	try {
	        		writer.write(str.toString() + "\n");
				} catch (IOException e) {
					e.printStackTrace();
				}
	        });
	      
	      writer.write("\nSorted by lambda\n\n");
	      writer.write("   Team Name                   won      drawn     lost     points\n");
	      writer.write("   ---------                   ---      ------    ----     ------\n");
	      table.stream().sorted((c1, c2) -> 
          ((Integer) c1.getPointsDifference()).compareTo(c2.getPointsDifference()))
	        .forEach(str -> {
	        	try {
	        		writer.write(str.toString() + "\n");
				} catch (IOException e) {
					e.printStackTrace();
				}
	        });
	      writer.close();
	      System.out.println("\nSuccessfully wrote to the file Output5.txt.");
	    } catch (IOException e) {
	      System.out.println("An error occurred.");
	      e.printStackTrace();
	    }
  }
}
