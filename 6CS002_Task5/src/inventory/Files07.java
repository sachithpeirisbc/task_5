package inventory;

import java.io.*;
import java.util.*;

public class Files07 {

  public static void main(String[] args) throws Exception {
    BufferedReader x  = 
      new BufferedReader(new FileReader("C:\\Users\\User\\eclipse-workspace\\wolf-fox.txt"));

    Optional <String >result = 
      x.lines()
       .reduce((left, right) -> left.concat(" ".concat(right)));
    
    if(result.isPresent())
      System.out.println("result is " + result.get());
    else
      System.out.println("result not present");
    x.close();
  }

}
