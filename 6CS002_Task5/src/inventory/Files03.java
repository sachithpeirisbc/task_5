package inventory;

import java.io.*;

public class Files03 {

  public static void main(String[] args) throws Exception {
    BufferedReader x = new BufferedReader(new FileReader("C:\\Users\\User\\eclipse-workspace\\wolf-fox.txt"));

    x.lines().filter(l -> l.contains("his"))
        .forEach(l -> System.out.println(l));

    x.close();
  }

}
