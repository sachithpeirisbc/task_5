package inventory;

import java.io.*;

public class Files04 {

  public static void main(String[] args) throws Exception {
    BufferedReader x  = 
        new BufferedReader(new FileReader("C:\\Users\\User\\eclipse-workspace\\wolf-fox.txt"));

    x.lines()
     .map(l -> l.toUpperCase())
     .forEach(l -> System.out.println(l));
    
    x.close();
  }

}
