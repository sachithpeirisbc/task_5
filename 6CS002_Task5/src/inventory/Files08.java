package inventory;

import java.io.*;
import java.util.*;
import java.util.stream.*;

public class Files08 {

  public static void main(String[] args) throws Exception {
    BufferedReader x  = 
        new BufferedReader(new FileReader("C:\\Users\\User\\eclipse-workspace\\wolf-fox.txt"));

    List<String> l = x.lines().collect(Collectors.toList());
    
    for(String line: l){
      System.out.println(line);
    }
    
    x.close();
  }

}
