package inventory;

import java.io.*;

public class Files05 {

  public static void main(String[] args) throws Exception {
    BufferedReader x = new BufferedReader(new FileReader("C:\\Users\\User\\eclipse-workspace\\wolf-fox.txt"));

    x.lines().sorted((a, b) -> {
      if (a.length() == b.length())
        return 0;
      if (a.length() < b.length())
        return 1;
      return -1;
    }).forEach(l -> System.out.println(l));

    x.close();
  }

}
