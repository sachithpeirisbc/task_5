package inventory;

import java.io.*;

public class Files06 {

  public static void main(String[] args) throws Exception {
    BufferedReader x  = 
       new BufferedReader(new FileReader("C:\\Users\\User\\eclipse-workspace\\wolf-fox.txt"));

    System.out.println(x.lines().reduce("", String::concat));
    
    x.close();
  }

}
